$fn=100;
length=135;
width=25;
hole_d=4.5;
hole_length=10;

module side_bar( hole_d=5, hole_length=10, hole_height=1.5 ) {
    translate([0,hole_d/2,0])
        cylinder( h=1.5, d=hole_d );   
    translate([0,hole_length-hole_d/2,0])
        cylinder( h=1.5, d=hole_d );   
    translate([-hole_d/2, hole_d/2,0])
        cube( [hole_d, hole_length-hole_d, 1.5] );

}


module strips_ring( kabel_d ) {
    translate( [0, 0, kabel_d-2 ] )
        rotate( [90, 0, 0]) {
        difference() {
            cylinder( h= 4, d=kabel_d+6 );
            cylinder( h= 5, d=kabel_d+3 );
        }
    }
}

module kabel_holder() {
    kabel_d=8;
    difference(){
        translate( [-(kabel_d+5)/2, -70/2, -1] )
            cube( [kabel_d+5, 70, 5] );
        translate( [0, 80/2, kabel_d/2+1] )
            rotate( [90, 0,0 ] )
                cylinder( d=kabel_d, h=80);
        translate( [0, -22, 0] )
            strips_ring(kabel_d);
        translate( [0, 20, 0] )
            strips_ring(kabel_d);
     }
    
}

module bundplade() {
    cube( [width, length, 2], center=true );
    cylinder( h=2, d=85, center=true );
}

difference() {
    bundplade();
    translate([0,0,-3]) 
        cylinder( h=5, d=hole_d-1.5 );        
}

difference(){
    translate([0,0,1]) 
        cylinder( h=1.5, d=hole_d );
    translate([0,0,0]) 
        cylinder( h=3, d=hole_d-1.5 );        
}

translate( [0,26.5,1] )
    side_bar( hole_d=5, hole_length=9.5 );

translate( [0,-26.5-hole_length,1] )
    side_bar( hole_d=5, hole_length=9.5 );

translate( [width/2+3, 0, 2] )
kabel_holder();

base_width=150; // length of support
height=15;      // height
depth=5;        // thickness

gevind_d=3.4;       // skrue gevind diameter
gevind_rod_h=4.9;   // skruer gevind længde
gevind_dybde_d=0.2; // hvor meget skruen skal skære i plastik

screw_head_h=3.1; // screw head height
screw_head_d=8.2; // screw head diameter

box_plastic_w=0.7;  // box plastic thickness
box_h=5;            // height og box plastik between parts

jitter=0.01; // visualization icing

$fn=64;

module base_shape( base_width, height, depth) {
linear_extrude( depth )
    polygon( points=[[0,0], [base_width,0], [base_width-height, height], [height,   height]]);
}

module inside_part() {
    difference()  {
        base_shape( base_width, height, depth );
        translate([base_width/2, height/2, -1*jitter])
            cylinder( d=gevind_d-gevind_dybde_d, h=gevind_rod_h*0.75+jitter*2);
        translate([base_width*5/6, height/2, -1*jitter])
            cylinder( d=gevind_d-gevind_dybde_d, h=gevind_rod_h*0.75+jitter*2);
        translate([base_width*1/6, height/2, -1*jitter])
            cylinder( d=gevind_d-gevind_dybde_d, h=gevind_rod_h*0.75+jitter*2);

        translate( [0, height-box_h, depth-box_plastic_w] )
            cube( [1.1*base_width, height-box_plastic_w, 3] );
    }
}

module outside_part() {
    difference()  {
        base_shape( base_width, height, depth );
        
        // skrue hul
        translate([base_width/2, height/2, -1*jitter])
            cylinder( d=gevind_d, h=depth+jitter*2);
        translate([base_width*5/6, height/2, -1*jitter])
            cylinder( d=gevind_d, h=depth+jitter*2);
        translate([base_width*1/6, height/2, -1*jitter])
            cylinder( d=gevind_d, h=depth+jitter*2);

        // nedsaenket skruehoved
        translate([base_width/2, height/2, -1*jitter])
            cylinder( d=screw_head_d, h=screw_head_h+jitter*2);
        translate([base_width*5/6, height/2, -1*jitter])
            cylinder( d=screw_head_d, h=screw_head_h+jitter*2);
        translate([base_width*1/6, height/2, -1*jitter])
            cylinder( d=screw_head_d, h=screw_head_h+jitter*2);

    }
}

//translate( [0, height*1.5, 0] )
//    outside_part();

inside_part();
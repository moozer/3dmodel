include <BOSL2/std.scad>
include <BOSL2/joiners.scad>
 
$fn=100;
height = 100;
width = 75;
corner_d = 5;
bottom_h = 2.5;
dt_depth = 3;
dt_female_tolerance = 10;
wall_depth = corner_d-dt_depth;
$slop=0.4;
//$slop=0.1;

module corner_pilar( h, d ) {
        cylinder( h=h, d=d, center=true );
}

module side( h, w, d )
{
    hull(){
        translate( [-w/2+d/2, 0, 0] )
            corner_pilar( h, d );
        translate( [w/2-d/2, 0, 0] )
            corner_pilar( h, d );
        translate( [0, -d/4, 0] )
            cube( [w, d/2, h], center=true );
    }
}

module side_male()
{
    translate( [0, corner_d/2-dt_depth/2, 0] )
    union() {

        translate( [0, 0, 0] )
            side( height, width, wall_depth );
        translate( [0, corner_d/2-dt_depth/2, 0] )
            rotate( [-90,0,0] )
                dovetail("male", slide=height, width=25, 
                           height=dt_depth, chamfer=1, slope=2, extra=0);
    }
}

module side_female()
{
    translate( [0, corner_d/2, 0] )
    difference() {
        translate( [0, 0, 0] )
            side( height,width, corner_d );
        translate( [0, corner_d/2, 0] )
            rotate( [-90,0,0] )
                dovetail("female", slide=height, width=25, 
                            height=dt_depth, chamfer=1, slope=2, extra=0.01);
    }
}


module pencil_case() {
translate( [0, -width/2+corner_d, 0] )
    rotate( [0, 0, 180] )
        side_female();
translate( [-width/2+corner_d, 0, 0] )
    rotate( [0, 0, 90] )
        side_female();
translate( [0, width/2-corner_d+dt_depth, 0] )
    side_male();
translate( [width/2-corner_d+dt_depth, 0, 0] )
    rotate( [0, 0, -90] )
        side_male();

// fix male-female corner
translate( [width/2-corner_d/2-(wall_depth)/2, -width/2+corner_d/2, 0] )
    cube( [corner_d, corner_d, height], center=true );

translate( [-width/2+corner_d/2, 
            width/2-corner_d/2-(wall_depth)/2, 0] )
    cube( [corner_d, corner_d, height], center=true );

color( "grey" )
translate( [dt_depth/2,dt_depth/2, height/2-bottom_h/2] )
    cube( [width-2*dt_depth, width-2*dt_depth, bottom_h], center=true );
}

pencil_case();

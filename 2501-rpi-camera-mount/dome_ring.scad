
$fn=100;

margin_w = 3;

disk_h = 3;

inner_bottom_d = 120.5;
outer_d = inner_bottom_d + margin_w*2;
inner_top_d = 99.5;

inner_l2_d = 115;
l2_h = 1.5;

rim_h = 3;


module ring( d1, d2, h ) {
difference() {
cylinder( d = d1, h=h, center=true );
cylinder( d = d2,  h=h*1.02, center=true );
}
}

// top ring
translate( [0,0,disk_h/2] )
ring(outer_d, inner_top_d, disk_h );

// l2 ring
translate( [0,0,disk_h+l2_h/2] )
ring(outer_d, inner_l2_d, l2_h );

// rim
translate( [0,0,disk_h+l2_h+rim_h/2] )
ring(outer_d, outer_d-margin_w*2, rim_h );
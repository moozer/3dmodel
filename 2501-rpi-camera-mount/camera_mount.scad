// post-build comment
// * the base plate should be thicker, e.g. 2mm
// * 2.5 mm holes on a thin plate are not 2.5mm

// see https://datasheets.raspberrypi.com/hq-camera/hq-camera-product-brief.pdf
module raspberry_hq_M12() {
    // all thickness is relative to base plate top
    base_size = 38;
    base_thickness = 1.5; //2.75;
    
    camera_diameter = 34.8;
    camera_thickness = 12.04;
    camera_top_diameter = 15.95;
    camera_top_thickness = 14.03;
    
    optics_diameter = camera_top_diameter-0.1;
    optics_thickness = camera_top_thickness + 10.0;
    
    protrusion_width = 13.97;
    protrusion_height = 12.04; //19;
    protrusion_thickness = 12.0; //; 5.71;
    
    slot_width = 21.0;
    slot_height = 2.75;
    slot_thickness = 7.0;

    hole_diameter = 2.5;
    hole_dist_to_edge = 4.0;
    hole_x = base_size/2-hole_dist_to_edge;

    // Base rectangle
    difference() {
        color("#252") 
            translate([-base_size / 2, -base_size / 2, -base_thickness])
                cube([base_size, base_size, base_thickness]);

        // Mounting holes
        translate([hole_x, hole_x, -1])
            cylinder(h = base_thickness + 1, d = hole_diameter, center = true);
        translate([-hole_x, hole_x, -1])
            cylinder(h = base_thickness + 1, d = hole_diameter, center = true);
        translate([hole_x, -hole_x, -1])
            cylinder(h = base_thickness + 1, d = hole_diameter, center = true);
        translate([-hole_x, -hole_x, -1])
            cylinder(h = base_thickness + 1, d = hole_diameter, center = true);
    }
    
    // Camera
    color("#333") 
    translate([0, 0, camera_thickness/2])
        cylinder(h = camera_thickness, d = camera_diameter, 
                    center = true);

    color("#333") 
    translate([0, 0, camera_top_thickness/2])
        cylinder(h = camera_top_thickness, d = camera_top_diameter, 
                    center = true);

    // fisheye optics
    color("#555") 
    translate([0, 0, optics_thickness/2])
        cylinder(h = optics_thickness, d = optics_diameter, 
                    center = true);

    // ribbon cable slot
    color("#fd9") 
    translate([base_size / 2 - slot_thickness, -slot_width / 2, -slot_height-base_thickness]) 
        cube([slot_thickness, slot_width,  slot_height]);

    // Protruding part
    color("#333") 
    translate([0, -protrusion_width/2, 0]) 
        cube([protrusion_thickness+base_size/2, protrusion_width, protrusion_height]);
    
}

$fn = 85;

base_size=38;
base_thickness=1.5;
hole_dist_to_edge = 4;
hole_diameter = 2.5;
hole_x = base_size/2-hole_dist_to_edge;
mount_height = 5;

module mount_corner_pad( screw_hole_d, mount_height ) 
{
    mount_hole_pad_d = 4;

    difference() {
        cylinder(h = mount_height, d = mount_hole_pad_d, center = true);
        cylinder(h = mount_height*1.02, d = screw_hole_d, center = true);    
    }
}
module mount_plate( ) {
    hole_x = 15;
    base_size = 38;
    mount_hole_pad_d = 4;
    mount_height = 5;
    mount_edge_padding = 0.5;
    mount_length = 60;
    
    plate_d = 85;
    plate_h = 1; 
    plate_screw_hole_x = plate_d/2-5;
    plate_screw_hole_d = 3;
    
    lift_h = 2;
    screw_hole_d = 1.9;
    
    ribbon_hole_depth = 2;
    ribbon_hole_width = 25;
    ribbon_hole_x = 35;

    color( "#f33" ) {
        
        // Pad circles for holes
        translate( [hole_x, hole_x, -mount_height/2] )
            mount_corner_pad( screw_hole_d, mount_height );
        translate( [-hole_x, hole_x, -mount_height/2] )
                mount_corner_pad( screw_hole_d, mount_height );
        translate( [-hole_x, -hole_x, -mount_height/2] )
                mount_corner_pad(screw_hole_d, mount_height);
        translate( [hole_x, -hole_x, -mount_height/2] )
                mount_corner_pad(screw_hole_d, mount_height);

    }

    translate( [0,0, -mount_height-plate_h/2 ])
    
    difference()  {
        // base plate
        cylinder( h=plate_h, d=plate_d, center=true );
 
        // center alignment hole
        cylinder( h=plate_h*1.02, d=2, center=true );

        // ribbon hole
        translate( [ ribbon_hole_x, 0 ,0] )
        hull() {
            translate( [0, ribbon_hole_width/2,0] )
            cylinder( h=plate_h*1.02, d=ribbon_hole_depth, center=true );
            translate( [0, -ribbon_hole_width/2,0] )
            cylinder( h=plate_h*1.02, d=ribbon_hole_depth, center=true );
        }

        // screw holes
        for( angle=[45, 135, 225, 315])
            rotate( [0,0,angle] )
                translate( [plate_screw_hole_x, 0, 0] )
                    cylinder( h=plate_h*1.02, d=plate_screw_hole_d, center=true );
    }
}


mount_plate();
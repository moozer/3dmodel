// rasperry mount

use <camera_mount.scad>;

// We already have a box for it with a vesa mount 4mm thick

$fn=185;
thickness = 2;

top_thickness = thickness; 
top_depth = 15;
top_width = 83.4+2*thickness; // VESA width all included + 2x2mm
top_holes_dist = 75/sqrt(2); 
//top_holes_dist = 70/sqrt(2); 

back_height = 130;
back_width = top_width;
back_band_width = 20;
back_thickness = thickness;

vesa_thickness = 4;

// top lid, for reference
module lid()
{
    difference() {
       cylinder( d=103, h = 3, center=true );
    // only the holes
    difference() {
       cylinder( d=84, h = 4, center=true );
       translate( [0,0,27] )
            scale( [1,1,5] )
                mount_plate();
    }
}
}

module top_plate()
{
    top_hole_d = 3;
    top_build_dist = 6;
    corner_cut_dist = 7;
    
    difference() {
        // top plate
        hull() {
            translate( [top_depth/2-back_thickness/2, top_width/2-top_build_dist/2, 0] )
                cube( [back_thickness, top_build_dist, top_thickness], center=true);
            translate( [top_depth/2-back_thickness/2, -top_width/2+top_build_dist/2, 0] )
                cube( [back_thickness, top_build_dist, top_thickness], center=true);
            translate( [-top_depth/2+top_build_dist/2,
                        -top_width/2+top_build_dist/2+corner_cut_dist, 
                        -top_thickness/2 ] )
                cylinder( d=top_build_dist, h=top_thickness);
            translate( [-top_depth/2+top_build_dist/2,
                        top_width/2-top_build_dist/2-corner_cut_dist, 
                        -top_thickness/2 ] )
                cylinder( d=top_build_dist, h=top_thickness);
//        #cube( [top_depth, top_width, top_thickness], center=true);

        }
    
        translate( [0,top_holes_dist/2,-top_thickness*1.02/2] )
            cylinder( d=top_hole_d, h=top_thickness*1.02);
        translate( [0,-top_holes_dist/2,-top_thickness*1.02/2] )
            cylinder( d=top_hole_d, h=top_thickness*1.02);
    }
}

module back_band( top_plate_w )
{
    bottom_support_t = 2;
    
    top_triangle_r = 2;
    top_triangle_h = top_plate_w-2*top_triangle_r-1;
    top_triangle_t = thickness;
    
    vesa_hole_d = 4;
    vesa_hole_z = 13; // from bottom
    vesa_hole_y = 5; // from edge
    
    top_hook_z = 96-4+0.1; // height minus width of hook plus some tolerance
    top_hook_width = 8;
    top_hook_l = 3;
    
    difference() {
        translate( [0,0,-bottom_support_t/2] )
        cube( [back_thickness, back_band_width, back_height+bottom_support_t], center=true);    
        
        // VESA hole
        translate( [-back_thickness/2*1.02, back_band_width/2-vesa_hole_y, 
                    -back_height/2+vesa_hole_z])
            rotate( [0,90,0])
                cylinder( d=vesa_hole_d, h=back_thickness*1.02);
    }
    
    // support at bottom
    translate( [bottom_support_t, 0, -back_height/2-bottom_support_t/2] )
        cube( [bottom_support_t, back_band_width, bottom_support_t], center=true);    
    
    // triangle at top for support
    translate( [-top_triangle_h-top_triangle_r/2, 
                    back_band_width/2-bottom_support_t-10,
                    back_height/2-top_triangle_r] )
    rotate( [-90, 0, 0] )
    hull() {
        cylinder(r=top_triangle_r, h=top_triangle_t);
        translate([top_triangle_h, top_triangle_h, 0]) 
            cylinder(r=top_triangle_r, h=top_triangle_t);
        translate([top_triangle_h, 0, 0]) 
            cylinder(r=top_triangle_r, h=top_triangle_t);
    }

    // hook for sliding in vesa
    translate([vesa_thickness,-top_hook_width+back_band_width/2,-back_height/2+top_triangle_r+top_hook_z]){
        rotate([-90,0,0])
        hull() {
            cylinder(r=top_triangle_r, h=top_hook_width);
            translate([top_hook_l+top_triangle_r*2, top_hook_l*2, 0]) 
                cylinder(r=top_triangle_r, h=top_hook_width);
            translate([top_hook_l, 0, 0]) 
                cylinder(r=top_triangle_r, h=top_hook_width);
        }
        translate([-top_triangle_r*2, 0, -top_triangle_r])
        cube( [vesa_thickness, top_hook_width, top_triangle_r*2] );
    }
 }

module bottom_plate()
 {
     bottom_height = 5;
     bottom_thickness = thickness;
     bottom_width = top_width;
     bottom_side_band_w=15;
     bottom_side_band_h=25;
     bottom_side_support_l=7;
     
     translate( [-bottom_thickness, 0, 0])
    {
     cube( [bottom_height+bottom_thickness, bottom_width, bottom_thickness] );
     cube( [bottom_side_band_h+bottom_thickness, bottom_side_band_w, bottom_thickness] );
       translate( [0,bottom_width-bottom_side_band_w,0] )
     cube( [bottom_side_band_h+bottom_thickness, bottom_side_band_w, bottom_thickness] );
    }
    
    // support beam I
translate( [-bottom_thickness/2, bottom_thickness, bottom_side_support_l+bottom_thickness/2] )
rotate( [90, 90, 0])
    hull() {
    cylinder( d=bottom_thickness, h=bottom_thickness );
    translate( [bottom_side_support_l, bottom_side_support_l, 0] )
    cylinder( d=bottom_thickness, h=bottom_thickness );
    }

    // support beam II
translate( [-bottom_thickness/2, bottom_width, bottom_side_support_l+bottom_thickness/2] )
rotate( [90, 90, 0])
    hull() {
    cylinder( d=bottom_thickness, h=bottom_thickness );
    translate( [bottom_side_support_l, bottom_side_support_l, 0] )
    cylinder( d=bottom_thickness, h=bottom_thickness );
    }

 }
 
//translate( [0,0,1.5] )
//    lid();

translate([top_depth/2-top_holes_dist/2,0,0])
union() { 
translate( [-top_depth/2, 0, -top_thickness/2] )
    top_plate();

// 2x back bands
translate( [-back_thickness/2, top_width/2-back_band_width/2, -back_height/2] )
    back_band(top_depth);
translate( [-back_thickness/2, -top_width/2+back_band_width/2, -back_height/2] )
    mirror([0,1,0])
        back_band(top_depth);
//    color("red")
    translate( [0, -top_width/2,-back_height-thickness] )
bottom_plate();
}
We made a foldable broadsword.

It was from [here](https://www.thingiverse.com/thing:3591373) on thingiverse.

It is very cool and printed in one piece. We did the test first to make sure that our setup was ok.

Printed on Monoprice Voxel using "fine" setting. It took 29 hours.
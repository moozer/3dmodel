module letter( filename, scale_factor ) {
    linear_extrude(height = 3)
    scale( [scale_factor, scale_factor,1])
        import(file=filename);
                
}

scale_f = 40/150;
distance = 50;


//translate( [49,-49,-2] )
//    %cube( [150, 150, 1], center=true );

translate( [0*distance, 0, 0] )
    letter( "Æ.dxf", scale_f);
translate( [1*distance, 0, 0] )
    letter( "D.dxf", scale_f);
translate( [2*distance, 0, 0] )
    letter( "E.dxf", scale_f);
translate( [0*distance, -distance, 0] )
    letter( "L.dxf", scale_f);
translate( [1*distance, -distance, 0] )
    letter( "L.dxf", scale_f);
translate( [2*distance, -distance, 0] )
    letter( "U.dxf", scale_f);
translate( [0*distance, -2*distance, 0] )
    letter( "N.dxf", scale_f);
translate( [1*distance, -2*distance, 0] )
    letter( "D.dxf", scale_f);
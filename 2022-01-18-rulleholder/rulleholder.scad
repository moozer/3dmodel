// fra https://openhome.cc/eGossip/OpenSCAD/SectorArc.html

module sector(radius, angles, fn = 24) {
    r = radius / cos(180 / fn);
    step = -360 / fn;

    points = concat([[0, 0]],
        [for(a = [angles[0] : step : angles[1] - 360]) 
            [r * cos(a), r * sin(a)]
        ],
        [[r * cos(angles[1]), r * sin(angles[1])]]
    );

    difference() {
        circle(radius, $fn = fn);
        polygon(points);
    }
}

module arc(radius, angles, width = 1, fn = 24) {
    difference() {
        sector(radius + width, angles, fn);
        sector(radius, angles, fn);
    }
} 

//linear_extrude(115) arc(radius, angles, width);


// my stuff
inner_r = 4.5; // diameter på pind + lidt
outer_r = 26; // diameter på rulle
wall_thickness = 1;
$fn = 96;
fn = 48; // resolution
height = 15; 
band_width = 10;    // width of the base
band_thickness = 1; // thickness of base   

module hjulholder() {
    // nederste plade
    difference() {
        cylinder( h=band_thickness, r=outer_r+band_width );
        cylinder( h=band_thickness, r=outer_r );
    };

    // rør, yderside
    //translate( [0, 0, 15] ) 
    difference() {
        cylinder( h=height, r=outer_r );
        cylinder( h=height, r=outer_r-wall_thickness );
    };


    linear_extrude( height, twist=100, slices=100 ) {
        for( i = [0:2] ) {
            arc( inner_r, [ 60+120*i, 120+120*i ], 
                 outer_r-inner_r-wall_thickness/2, fn=fn );
        }
    }

    difference() {
            cylinder( h=height, r=inner_r+wall_thickness );
            cylinder( h=height, r=inner_r );
    }
};

hjulholder();
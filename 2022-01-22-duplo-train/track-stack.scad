include <lego_duplo_v2.scad>

GenerateObject=0; // to not draw test objects

stack( 10 );


/// -- modules below

// extra_block_padding is the extra depth/width at the back for stability
module base_shape( extra_block_padding = 5, unitsX=1, unitsY = 4, jitter=0.5 ) {
    difference() {
        union() {
            //translate([Unit/2,0,0])
            translate([0,-unitsY*Unit/2, 0])
                square([unitsX*Unit+extra_block_padding, unitsY*Unit] );
            translate([-Unit/4,-Unit/2,0])   
                square([Unit/2,TrackPinWidth-jitter],center=true);
            translate([-Unit/2,-Unit/2,0])   
                circle(d=TrackPin-jitter);
                  
        }
        translate([Unit/4,Unit/2,0])   
            square([Unit/2,TrackPinWidth+jitter], center=true);
        translate([Unit/2,Unit/2,0])   
            circle(d=TrackHole+jitter);
    }   
}

module stack ( height_count = 2 ) {
    // 4x base size
    unitsX=1;
    unitsY=4;

    // extra size for stability
    extra_block_padding = 5;

    // no of tracks
    height_per_track = 13.9;

    linear_extrude( height_per_track *height_count ) {
        base_shape( extra_block_padding = extra_block_padding, unitsX=unitsX, unitsY=unitsY );
    }

    // and add a base plate
    linear_extrude( 1 )
    {
        difference() {
            circle(d=unitsY*Unit);
               translate([0,-unitsY*Unit/2,0 ])
                square( unitsY*Unit );
        }
        translate([0,-(unitsY*Unit)/2, 0])
            square([unitsX*Unit+extra_block_padding, unitsY*Unit]);

    }

}


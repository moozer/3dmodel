width = 110;
height = 140-width/2;
neck_w = 20;
neck_h = 30;
neck_support_d = 40; 
base_w = 100;
base_h = 3;
thickness = 3;
base_ring_d = base_w - 10;


module baloon_cutout() {

    translate( [0,-height,0] ) {
        difference() {
            circle( d=width );
            translate( [-width/2, 0, 0] )
                square( [width, width/2] );
        }
        polygon([[-width/2, 0],[width/2, 0],[0, height-neck_h+neck_w]]);

        translate( [0, height-neck_h/2, 0])
            square( [neck_w, neck_h], center=true );

        translate( [0, height-base_h/2, 0])
            square( [base_w, base_h], center=true );
        
        difference() {
            translate( [neck_support_d/4+neck_w/2,
                        height-base_h-neck_support_d/2+neck_support_d/4,0] )
                square( [neck_support_d/2, neck_support_d/2], center=true );        
            translate( [neck_support_d/2+neck_w/2,height-base_h-neck_support_d/2,0] )
                circle( d=neck_support_d );
        }

        difference() {
            translate( [-neck_support_d/4-neck_w/2,
                        height-base_h-neck_support_d/2+neck_support_d/4,0] )
                square( [neck_support_d/2, neck_support_d/2], center=true );        
            translate( [-neck_support_d/2-neck_w/2,height-base_h-neck_support_d/2,0] )
                circle( d=neck_support_d );
        }
    }
}

linear_extrude( height=thickness, center=true )
    baloon_cutout();
rotate( [0,90,0])
linear_extrude( height=thickness, center=true )
    baloon_cutout();
//rotate( [0,120,0])
//    linear_extrude( height=thickness, center=true )
//        baloon_cutout();
//
//translate([0, -thickness/2, 0])
//    rotate([90, 0,0])
//        difference() {
//            cylinder( h=thickness, d=base_ring_d, center=true );
//            cylinder( h=thickness*1.02, d=base_ring_d-thickness, center=true );
//        }
// 
include <BOSL2/std.scad>
include <BOSL2/screws.scad>

// if left to default, the plate will be slightly narrower than mount_arm_w
$fa = 1;

antenna_height=61;
antenna_outer_r=34.5/2;
antenna_inner_r=26/2+0.1;
antenna_outer_point_l=antenna_outer_r+2;
antenna_outer_point_w=17.0;
antenna_dome_r=97/2;

antenna_scale_factor = 1.01;

mount_arm_l = antenna_dome_r+20;
mount_arm_w = antenna_outer_r*2+10;
mount_arm_h = 20;

cable_d = 5.1;
cable_cut_d = 90;
cable_cut_l = 25; 

cable_tie_d = 12;

module antenna() {
    cylinder( h=antenna_height, r=antenna_outer_r, center=true);
    translate( [antenna_outer_point_l/2, 0, 0] ) 
        cube( [ antenna_outer_point_l, antenna_outer_point_w, antenna_height], center=true );
}

module mount_arm() {
    difference() {
        union() {
            cube( [mount_arm_l, mount_arm_w, mount_arm_h ]);
            translate( [mount_arm_l, mount_arm_w/2, 0] )
                cylinder( d=mount_arm_w, h=mount_arm_h );
        }
        translate( [mount_arm_l, mount_arm_w/2, antenna_height/2+mount_arm_h/2] )
            rotate( [0, 0, 180 ] )
                scale([antenna_scale_factor,antenna_scale_factor,antenna_scale_factor]) 
                    antenna();
        translate( [mount_arm_l, mount_arm_w/2, -mount_arm_h/2] )
            cylinder( r=antenna_inner_r, h=mount_arm_h*2 );
        // cable cutout
        translate( [cable_cut_l, mount_arm_w+cable_cut_d/2-cable_d/2, mount_arm_h/2] ) 
            cylinder( h=cable_d, d=cable_cut_d, center=true );
    // cable tie hole
    translate( [cable_cut_l, mount_arm_w-cable_tie_d/2+cable_d, mount_arm_h/2] ) 
        rotate( [ 0, 90, 0] ) {
        difference() {
         cylinder( h=cable_d, d=cable_tie_d, center=true );
         cylinder( h=cable_d+0.01, d=cable_tie_d-3, center=true );
        }
    }

    }
}

module mount_plate() {
    translate( [0, mount_arm_w/2, -mount_arm_l/4] ) {
        difference() {
            // actual back plate
            hull() {
                rotate( [0, 90, 0] )
                    cylinder( d=mount_arm_w, h=mount_arm_h, center=true );

                translate( [0, 0, mount_arm_l] )
                    rotate( [0, 90, 0] )
                        cylinder( d=mount_arm_w, h=mount_arm_h, center=true );
                
                // an extra cube is added to handle minuscule missing width
                // of the back plate due to how "hull" works
                translate( [0, 0, mount_arm_l/2] )
                    cube( [mount_arm_h, mount_arm_w, mount_arm_h*2], center=true );
                }

            // two holes for screws
            translate( [mount_arm_h/2, 0, 0] )
                rotate( [0, 90, 0] )
                    screw_hole("M4,40",head="flat",counterbore=0,anchor=TOP);
                
            translate( [mount_arm_h/2, 0, mount_arm_l] )
                rotate( [0, 90, 0] )
                    screw_hole("M4,40",head="flat",counterbore=0,anchor=TOP);
                }
    }
}

mount_plate();
mount_arm();


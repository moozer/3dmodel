include <BOSL2/std.scad>
include <BOSL2/screws.scad>

spec = [["system","ISO"],
        ["type","screw_info"],
        ["pitch", 1.814],
        ["head", "hex"],
        ["head_size", 36],
        ["head_height", 10],
        ["diameter",26],
        ["thread_len", 25],
       ["length",35]];
screw(spec,tolerance=0);

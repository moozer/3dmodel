outline="skeetch.dxf";

height=20;

scale_factor=0.25;
translate_y=440;

linear_extrude( height )
minkowski(){
        translate([0, -translate_y*scale_factor,0])
            scale( [scale_factor, scale_factor, 1])
                import(file=outline);
    circle( d=height );
}

translate( [160*scale_factor, 80*scale_factor, 0] )
difference() {
cylinder( h=height, d=20 );
cylinder( h=height, d=10 );


}

use <rsquare.scad>;

plate_height = 100*0.6;
plate_width = 150;
plate_depth = 2;
//font = "Liberation Sans"
font = "Mono slanted 10";

text_depth = 3;


color( [1, 0, 1] )
linear_extrude( plate_depth )

    translate( [-plate_width/2, plate_height/2, 0] )
    rotate( [0, 0, -90] )
            rsquare( [plate_height, plate_width], 10 );

color( [1,1,1] )
translate( [0,0, plate_depth] )
linear_extrude( text_depth ) {
        translate( [ 0, 0, 0 ] )
            text("Karen", size = plate_height-30, font = font,
                halign = "center", valign = "center", $fn = 16);


    color( [1, 0.4, 0.4] )
    translate( [-plate_width/2, plate_height/2, 0] )
    rotate( [0, 0, -90] )
    difference() {
        translate( [5, 5, 0] )
            rsquare( [plate_height-10, plate_width-10], 10 );
        translate( [10, 10, 0] )
            rsquare( [plate_height-20, plate_width-20], 10 );
    };

};


module rsquare(size, radius) {
    hull() {
        translate([radius, radius]) circle(r = radius);
        translate([size[0] - radius, radius]) circle(r = radius);
        translate([size[0] - radius, size[1] - radius]) circle(r = radius);
        translate([radius, size[1] - radius]) circle(r = radius);
    }
}
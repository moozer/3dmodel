// fra https://openhome.cc/eGossip/OpenSCAD/SectorArc.html

module sector(radius, angles, fn = 24) {
    r = radius / cos(180 / fn);
    step = -360 / fn;

    points = concat([[0, 0]],
        [for(a = [angles[0] : step : angles[1] - 360]) 
            [r * cos(a), r * sin(a)]
        ],
        [[r * cos(angles[1]), r * sin(angles[1])]]
    );

    difference() {
        circle(radius, $fn = fn);
        polygon(points);
    }
}

module arc(radius, angles, width = 1, fn = 24) {
    difference() {
        sector(radius + width, angles, fn);
        sector(radius, angles, fn);
    }
} 


module nav( inner_r, width, inner_offset, fn=24 ) {
    difference() {
        arc( inner_r, [ 60, 120 ], 
                width, fn=fn );
        translate( [0, inner_offset, 0] ) 
            arc( inner_r, [ 60, 120 ], 
                width, fn=fn );    
    }
};

module nodge( n_height ) {
    hull() {
        translate( [0, 0, n_height ] ) 
            sphere( r = 1 );
        translate( [0, 0, -n_height ] ) 
            sphere( r = 1 );
    }    
}


// my stuff
inner_r = 4.5;      // radius på indre "stav"
outer_r = 75/2;   // radius på rulle
plate_width = 10;
plate_height = 1.5;
wall_thickness = 5;    // tykkelse på væg
$fn = 100;
fn = $fn; // resolution
height = 25; 
height_add = 0.05;
nodge_count = 24;


module make_object() {
    // nederste ring
    translate( [0, 0, plate_height/2] ) difference() {
        cylinder( h=plate_height, r=outer_r+plate_width, center=true );
        cylinder( h=plate_height+height_add, r=outer_r, center=true );
    };

    // rør, yderside
    translate( [0, 0, height/2] ) difference() {
        cylinder( h=height, 
                    r=outer_r, center=true );
        cylinder( h=height+height_add, 
                    r=outer_r-wall_thickness, center=true );
    };


    // nav stivere
    linear_extrude( height, twist=20, slices = 50 ) {
        for( i = [0:2] ) {
            rotate( [0, 0, 120*i] ) 
                nav( inner_r, outer_r-inner_r-wall_thickness,
                    wall_thickness, fn=fn );
        }
    }

    // base plader
    linear_extrude( plate_height ) {
        for( i = [0:2] ) {
            rotate( [0, 0, 120*i] ) 
                translate( [0, wall_thickness, 0] ) 
                    arc( inner_r, [ 60-2, 120 ], 
                        outer_r-inner_r-wall_thickness, fn=fn );
        }
    };
    // indre rør
    difference() {
        translate( [0, 0, height/2] ) 
            cylinder( h=height, r=inner_r+wall_thickness, 
                        center=true );
        cylinder( h=height/2, r=2 );
    };

    // udposninger på ydersiden
    for( i = [0: nodge_count-1] ) {
        rotate( [0, 0, 360/nodge_count*i] )
            translate( [outer_r, 0, height/2] )
                nodge( height*0.65/2 );
    }
}

make_object();
